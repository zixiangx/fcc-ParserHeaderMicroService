﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RequestHeaderParserMicroService.Interfaces
{
    interface IHeaderParsingService
    {
        string GetIpAddress();
        string GetBrowserLanguage();
        string GetOperatingSystem();

    }
}
