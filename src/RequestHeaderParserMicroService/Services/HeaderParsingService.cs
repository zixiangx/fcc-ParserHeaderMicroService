﻿using RequestHeaderParserMicroService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace RequestHeaderParserMicroService.Services
{
    public class HeaderParsingService : IHeaderParsingService
    {
        private HttpRequest request;

        public HeaderParsingService(HttpRequest request)
        {
            this.request = request;
        }

        public string GetBrowserLanguage()
        {
            const string ACCEPT_LANGUAGE = "Accept-Language";
            string languageString = request.Headers.SingleOrDefault(f => f.Key == ACCEPT_LANGUAGE).Value;
            
            if (languageString.IndexOf(',') >= 0)
                languageString = languageString.Substring(0, languageString.IndexOf(','));

            return languageString;
        }

        public string GetIpAddress()
        {
            return request.HttpContext.Connection.RemoteIpAddress.ToString();
        }

        public string GetOperatingSystem()
        {
            const string USER_AGENT = "User-Agent";

            string operatingSystem = null;
            string userAgent = request.Headers.SingleOrDefault(f => f.Key == USER_AGENT).Value;


            if (userAgent.IndexOf("Win16") >= 0)
            {
                operatingSystem = "Windows 3.11";
            }
            else if (userAgent.IndexOf("Win95") >= 0 || userAgent.IndexOf("Windows 95") >= 0 || userAgent.IndexOf("Windows_95") >= 0)
            {
                operatingSystem = "Windows 95";
            }
            else if (userAgent.IndexOf("Win98") >= 0 || userAgent.IndexOf("Windows 98") >= 0)
            {
                operatingSystem = "Windows 98";
            }
            else if (userAgent.IndexOf("Windows NT 5.0") >= 0 || userAgent.IndexOf("Windows 2000") >= 0)
            {
                operatingSystem = "Windows 2000";
            }
            else if (userAgent.IndexOf("Windows NT 5.1") >= 0 || userAgent.IndexOf("Windows XP") >= 0)
            {
                operatingSystem = "Windows XP";
            }
            else if (userAgent.IndexOf("Windows NT 5.2") >= 0)
            {
                operatingSystem = "Windows Server 2003";
            }
            else if (userAgent.IndexOf("Windows NT 6.0") >= 0)
            {
                operatingSystem = "Windows Vista";
            }
            else if (userAgent.IndexOf("Windows NT 6.1") >= 0)
            {
                operatingSystem = "Windows 7";
            }
            else if (userAgent.IndexOf("Windows NT 6.2") >= 0)
            {
                operatingSystem = "Windows 8";
            }
            else if (userAgent.IndexOf("Windows NT 10.0") >= 0)
            {
                operatingSystem = "Windows 10";
            }
            else if (userAgent.IndexOf("Windows NT 4.0") >= 0 || userAgent.IndexOf("WinNT4.0") >= 0 || userAgent.IndexOf("WinNT") >= 0 || userAgent.IndexOf("Windows NT") >= 0)
            {
                operatingSystem = "Windows NT 4.0";
            }
            else if (userAgent.IndexOf("Windows ME") >= 0)
            {
                operatingSystem = "Windows ME";
            }
            else if (userAgent.IndexOf("OpenBSD") >= 0)
            {
                operatingSystem = "Open BSD";
            }
            else if (userAgent.IndexOf("SunOS") >= 0)
            {
                operatingSystem = "Sun OS";
            }
            else if (userAgent.IndexOf("Linux") >= 0 || userAgent.IndexOf("X11") >= 0)
            {
                operatingSystem = "Linux";
            }
            else if (userAgent.IndexOf("Mac_PowerPC") >= 0 || userAgent.IndexOf("Macintosh") >= 0)
            {
                operatingSystem = "Mac OS";
            }
            else if (userAgent.IndexOf("QNX") >= 0)
            {
                operatingSystem = "QNX";
            }
            else if (userAgent.IndexOf("BeOS") >= 0)
            {
                operatingSystem = "BeOS";
            }
            else if (userAgent.IndexOf("OS/2") >= 0)
            {
                operatingSystem = "OS/2";
            }

            return operatingSystem;
        }
    }
}
