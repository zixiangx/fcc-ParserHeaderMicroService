This is an implementation of a Header Parsing MicroService API that processes
the header information of the client to display the user's IP Address, Browser
Language and also Operating System.

It is built on the .NET Core platform.

The live demo can be found at
http://zx-headerparsingmicroservice.azurewebsites.net/api/Whoami