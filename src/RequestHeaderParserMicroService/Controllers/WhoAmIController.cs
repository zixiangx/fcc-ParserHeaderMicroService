﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RequestHeaderParserMicroService.Services;
using Newtonsoft.Json;

namespace RequestHeaderParserMicroService.Controllers
{
    [Route("api/[controller]")]
    public class WhoamiController : Controller
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            HeaderParsingService _parsingService = new HeaderParsingService(Request);

            string ipAddress = _parsingService.GetIpAddress();
            string language = _parsingService.GetBrowserLanguage();
            string operatingSystem = _parsingService.GetOperatingSystem();

            Dictionary<string, string> result = new Dictionary<string, string>();

            result.Add("ipAddress", ipAddress);
            result.Add("language", language);
            result.Add("software", operatingSystem);
            
            return Content(JsonConvert.SerializeObject(result), "application/json");
        }
    }
}
